import { GET_ALL_PRODUCTS, GET_CART_PRODUCTS, ADD_PRODUCT, REMOVE_PRODUCT, SET_CURRENT_PRODUCT } from './type';
import axios from 'axios';
import products from './products.json';

export let getAllProducts = () => {
    return (dispatch) => {
        if (products) {
            dispatch(allProducts(products));
        }
    }
}

export let setCurrentProduct = (data) => {
    return {
        type: SET_CURRENT_PRODUCT,
        data
    }
}

allProducts = (data) => {
    return {
        type: GET_ALL_PRODUCTS,
        data,
    }
}

cartProducts = () => {
    return {
        type: GET_CART_PRODUCTS,
    }
}

export let addProduct = (data) => {
    return {
        type: ADD_PRODUCT,
        data,
    }
}

export let removeProduct = (data) => {
    return {
        type: REMOVE_PRODUCT,
        data,
    }
}