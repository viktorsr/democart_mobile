import { GET_ALL_PRODUCTS, ADD_PRODUCT, REMOVE_PRODUCT, SET_CURRENT_PRODUCT } from '../actions/type';

const initialState = {
    allProducts: [],
    cartProducts: [],
    cartTotal: 0.00,
    currentProduct: [],
    isLoading: false,
}

CartReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_PRODUCTS: {
            return {
                ...state,
                allProducts: action.data.sort((a, b) => { return a.id - b.id }),
            }
        }

        case SET_CURRENT_PRODUCT: {
            let currentProduct = state.allProducts.find(product => action.data === product.id);
            // console.log(currentProduct);
            return {
                ...state,
                currentProduct,
            }
        }

        case ADD_PRODUCT: {
            let newProduct = state.allProducts.find(product => action.data === product.id);
            newProduct['inCart'] = true;
            let allProducts = state.allProducts.filter(product => action.data !== product.id);
            // console.log(newProduct);
            if (state.cartProducts.find(product => action.data === product.id)) {
                return state;
            } else {
                return {
                    ...state,
                    cartTotal: (state.cartTotal * 1 + newProduct.price * 1),
                    cartProducts: [...state.cartProducts, newProduct],
                    allProducts: [...allProducts, newProduct].sort((a, b) => { return a.id - b.id }),
                    currentProduct: newProduct,
                }

            }
        }

        case REMOVE_PRODUCT: {
            let cartProducts = state.cartProducts.filter(product => action.data !== product.id);
            let product = state.allProducts.find(product => action.data === product.id);
            let allProducts = state.allProducts.filter(product => action.data !== product.id);
            if (product.inCart == false) {
                return state;
            } else {
                product['inCart'] = false;
                return {
                    ...state,
                    cartTotal: (state.cartTotal * 1 - product.price * 1),
                    cartProducts,
                    allProducts: [...allProducts, product].sort((a, b) => { return a.id - b.id }),
                    currentProduct: product,
                }
            }
        }

        default:
            return state;
    }
}

export default CartReducer;