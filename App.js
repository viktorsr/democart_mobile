import React from 'react';
import { createAppContainer, createDrawerNavigator, createStackNavigator } from 'react-navigation';
import { Provider as PaperProvider } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import { Provider } from 'react-redux';
import configureStore from './redux/store/index';

const store = configureStore();

import HomeScreen from './screens/HomeScreen';
import CartScreen from './screens/CartScreen';
import ProductScreen from './screens/ProductScreen';

const drawerNavigator = createDrawerNavigator(
	{
		Home: {
			screen: HomeScreen,
			navigationOptions: ({ navigation }) => ({
				drawerLabel: 'Products',
				drawerIcon: () => (
					<Icon name='md-home' size={20} />
				)
			})
		},
		Cart: {
			screen: CartScreen,
			navigationOptions: ({ navigation }) => ({
				drawerLabel: 'Cart',
				drawerIcon: () => (
					<Icon name='md-cart' size={20} />
				)
			})
		},
	}, {
		drawerType: 'slide',
	}
);

const StackNavigator = createAppContainer(createStackNavigator(
	{
		Main: {
			screen: drawerNavigator,
			navigationOptions: {
				header: null,
			}
		},
		Product: {
			screen: ProductScreen,
			navigationOptions: {
				title: 'Apple',
			}
		}

	},
	{
		initialRouteName: 'Main',
	}
));


const App = () => {
	return (
		<Provider store={store}>
			<PaperProvider>
				<StackNavigator />
			</PaperProvider>
		</Provider>
	)
}


export default App;
