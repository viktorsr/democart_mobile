import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { Appbar, Text } from 'react-native-paper';
import styles from './components/styles';
import { connect } from 'react-redux';
import CartProduct from './components/CartProduct';

import { removeProduct } from '../redux/actions';

class CartScreen extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };
    }

    _onPressProduct = ({ id }) => {
        this.props.navigation.navigate('Product', { id });
    }

    _removeProductFromCart = ({ id }) => {
        console.log(id);
        this.props.removeFromCart(id);
    }

    _renderItem = ({ item }) => (
        <CartProduct
            onPressRemoveProduct={this._removeProductFromCart}
            onPressProduct={this._onPressProduct}
            item={item}
        />

    )


    render () {
        return (
            <View style={styles.container}>
                <Appbar style={styles.header}>
                    <Appbar.Action icon="more-vert" onPress={() => this.props.navigation.openDrawer()} />
                    <Appbar.Content title="Cart" subtitle="" />
                </Appbar>
                <FlatList
                    data={this.props.products}
                    keyExtractor={(item, index) => item.id}
                    renderItem={this._renderItem}
                />
                <Text style={styles.totalPrice}>Total: {this.props.cartTotal.toFixed(2)}</Text>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeFromCart: (id) => dispatch(removeProduct(id)),
    }
}

const mapStateToProps = (state) => {
    const products = state.cart.cartProducts;
    const cartTotal = state.cart.cartTotal;
    return { products, cartTotal };
}

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);
