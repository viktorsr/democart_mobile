import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { Appbar, Text } from 'react-native-paper';
import styles from './components/styles';
import { connect } from 'react-redux';
import { getAllProducts, addProduct, removeProduct, setCurrentProduct } from '../redux/actions';
import Product from './components/Product';

class HomeScreen extends Component {
    constructor (props) {
        super(props);
        this.state = {

        };
        this.props.getProducts();
    }

    _onPressProduct = ({ id }) => {
        this.props.setCurrentProduct(id);
        this.props.navigation.navigate('Product');
    }

    _addProductToCart = ({ id }) => {
        console.log(id);
        this.props.addToCart(id);
    }

    _removeProductFromCart = ({ id }) => {
        console.log(id);
        this.props.removeFromCart(id);
    }

    _renderItem = ({ item }) => (
        <Product
            onPressProduct={this._onPressProduct}
            onPressAddProduct={this._addProductToCart}
            onPressRemoveProduct={this._removeProductFromCart}
            item={item}
        />

    )

    render () {
        return (
            <View style={styles.container}>
                <Appbar style={styles.header}>
                    <Appbar.Action icon="more-vert" onPress={() => this.props.navigation.openDrawer()} />
                    <Appbar.Content title="All Products" subtitle="" />
                </Appbar>
                <FlatList
                    data={this.props.products}
                    keyExtractor={(item, index) => item.id}
                    renderItem={this._renderItem}
                />
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getProducts: () => dispatch(getAllProducts()),
        addToCart: (id) => dispatch(addProduct(id)),
        removeFromCart: (id) => dispatch(removeProduct(id)),
        setCurrentProduct: (id) => dispatch(setCurrentProduct(id)),
    }
}

const mapStateToProps = (state) => {
    const products = state.cart.allProducts;
    return { products };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
