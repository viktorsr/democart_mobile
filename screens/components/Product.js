import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Card, Button, Title, Paragraph, Text } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class Product extends Component {
	constructor (props) {
		super(props);
		this.state = {
		};
	}

	render () {
		const { id, title, description, inCart, price } = this.props.item;
		return (
			<Card>
				<TouchableOpacity onPress={() => this.props.onPressProduct({ id })}>
					<Card.Cover source={{ uri: 'https://kursors.lv/wp-content/uploads/2017/09/iphones8-plus-x.jpg' }} />
					<Card.Content>
						<Title>{title}</Title>
						<Paragraph>{description}</Paragraph>
						<Text style={styles.price}>$ {(price * 1).toFixed(2)}</Text>
					</Card.Content>
				</TouchableOpacity>
				<Card.Actions>
					{inCart == false ?
						<Button onPress={() => this.props.onPressAddProduct({ id })}><Icon name="md-cart" size={16} /> Add to cart</Button>
						:
						<Button onPress={() => this.props.onPressRemoveProduct({ id })}><Icon name="md-cart" size={16} /> Remove from cart</Button>
					}
				</Card.Actions>
			</Card>
		);
	}
}

export default Product;
