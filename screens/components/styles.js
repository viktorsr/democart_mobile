import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');

export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    header: {
        backgroundColor: '#b3b3b3',
    },

    price: {
        fontSize: 18,
        alignSelf: 'flex-end',
        margin: 10,
    },

    totalPrice: {
        fontSize: 16,
        alignSelf: 'center',
        marginVertical: 5,
    },

    image: {
        width: win.width,
        height: 450 * win.width / 800,
    },

    description: {
        fontSize: 16,
        padding: 5,
    },

    title: {
        fontWeight: 'bold',
        fontSize: 20,
    },


});