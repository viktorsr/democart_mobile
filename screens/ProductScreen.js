import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Appbar, Text, Paragraph, Button } from 'react-native-paper';
import styles from './components/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { addProduct, removeProduct } from '../redux/actions';

class ProductScreen extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };

    }

    _addProductToCart = ({ id }) => {
        this.props.addToCart(id);
    }

    _removeProductFromCart = ({ id }) => {
        this.props.removeFromCart(id);
    }


    render () {
        const { id, description, title, price } = this.props.currentProduct;
        return (
            <View style={styles.container} >
                <Image source={{ uri: 'https://kursors.lv/wp-content/uploads/2017/09/iphones8-plus-x.jpg' }} style={styles.image} />
                <Text style={styles.title}>{title}</Text>
                <Paragraph style={styles.description}>{description}</Paragraph>
                <Text style={styles.price}>$ {(price * 1).toFixed(2)}</Text>
                {this.props.inCart == false ?
                    <Button onPress={() => this._addProductToCart({ id })}><Icon name="md-cart" size={16} /> Add to cart</Button>
                    :
                    <Button onPress={() => this._removeProductFromCart({ id })}><Icon name="md-cart" size={16} /> Remove from cart</Button>
                }
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (id) => dispatch(addProduct(id)),
        removeFromCart: (id) => dispatch(removeProduct(id)),
    }
}

const mapStateToProps = (state, props) => {
    const currentProduct = state.cart.currentProduct;
    const inCart = state.cart.currentProduct.inCart;
    return { currentProduct, inCart };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductScreen);
